package com.bathuni.emotivrccar.androidcontroller;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortOut;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    // Camera and sending
    WebView webView;
    OSCPortOut sender;

    // Camera controls
    boolean camera = false;
    Button camBtn;

    // Directions
    int FOWARD = 0;
    int BACKWARD = 1;
    int LEFT = 2;
    int RIGHT = 3;
    int STOP = 4;
    // Commands
    int END = 5;
    // Camera
    int UP = 6;
    int DOWN = 7;
    int PANLEFT = 8;
    int PANRIGHT = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_main);
        webView = (WebView) findViewById(R.id.carStream);
        camBtn = (Button) findViewById(R.id.camBtn);
        camBtn.setBackgroundColor(Color.RED);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void startVideo(View view) {
        // Get Ip address and get video address
        EditText editText = (EditText) findViewById(R.id.ipAddressTxt);
        String ipAddress = editText.getText().toString();
        String vidAddress = "http://" + ipAddress + ":8080/?action=stream";

        // get vid uri and set video view to this
        webView.loadUrl(vidAddress);
        WebSettings webSettings = webView.getSettings();
        webSettings.setDisplayZoomControls(true);
        webView.zoomOut();

        try {
            InetAddress oscAddress = InetAddress.getByName(ipAddress);
            // create OSC sender
            try {
                sender = new OSCPortOut(oscAddress, 7400);
            } catch (SocketException e) {
                e.printStackTrace();
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public void camBtnClicked(View view) {
        if (camera){
            camera = false;
            camBtn.setBackgroundColor(Color.RED);
        }else{
            camera = true;
            camBtn.setBackgroundColor(Color.GREEN);
        }
    }

    public void fowardBtnClicked(View view) {
        if (camera){
            OSCMessage msg = new OSCMessage("/camera");
            msg.addArgument(UP);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            OSCMessage msg = new OSCMessage("/direction");
            msg.addArgument(FOWARD);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void backwardBtnClicked(View view) {
        if (camera){
            OSCMessage msg = new OSCMessage("/camera");
            msg.addArgument(DOWN);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            OSCMessage msg = new OSCMessage("/direction");
            msg.addArgument(BACKWARD);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void leftBtnClicked(View view) {
        if (camera){
            OSCMessage msg = new OSCMessage("/camera");
            msg.addArgument(PANLEFT);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            OSCMessage msg = new OSCMessage("/direction");
            msg.addArgument(LEFT);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void rightBtnClicked(View view) {
        if (camera){
            OSCMessage msg = new OSCMessage("/camera");
            msg.addArgument(PANRIGHT);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            OSCMessage msg = new OSCMessage("/direction");
            msg.addArgument(RIGHT);
            try {
                sender.send(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stopBtnClicked(View view) {
        OSCMessage msg = new OSCMessage("/direction");
        msg.addArgument(STOP);
        try {
            sender.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void endBtnClicked(View view) {
        OSCMessage msg = new OSCMessage("/command");
        msg.addArgument(END);
        try {
            sender.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
